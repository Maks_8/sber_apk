import React from 'react'
import { StatusBar } from 'expo-status-bar'
import { Provider } from 'react-redux'
import { store } from './src/redux'
import { useFonts } from 'expo-font'

import { colors } from './src/styles'
import AppView from './src/view/AppView'

export default function App() {
  const [fontsLoaded] = useFonts({
    'SBSansRegular': require('./assets/fonts/SBSansText-Regular.ttf'),
    'SBSansBold': require('./assets/fonts/SBSansText-Bold.ttf'),
  })

  if (!fontsLoaded) {
    return null
  }

  return (
    <Provider store={store}>
      <StatusBar style="dark" backgroundColor={colors.white} />
      <AppView />
    </Provider>
  )
}
