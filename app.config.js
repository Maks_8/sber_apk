import 'dotenv/config'

export default ({ config }) => {
  return {
    ...config,
    version: process.env.VERSION,
    android: {
      package: process.env.PACKAGE_NAME,
      versionCode: +process.env.ANDROID_VERSION,
      adaptiveIcon: {
        foregroundImage: './assets/icon.png',
        backgroundColor: '#FFFFFF',
      },
      // config: {
      //   googleMaps: {
      //     apiKey: process.env.GOOGLE_MAPS_API,
      //   },
      // },
      // useNextNotificationsApi: true,
      permissions: [],
    },
    ios: {
      supportsTablet: true,
      bundleIdentifier: process.env.PACKAGE_NAME,
      buildNumber: process.env.VERSION,
      usesIcloudStorage: true,
      // config: {
      //   googleMapsApiKey: process.env.GOOGLE_MAPS_API,
      // },
      // infoPlist: {
      //   NSCameraUsageDescription:
      //     'This app uses the camera to make photos of documents and attach it to the task.',
      //   NSPhotoLibraryUsageDescription:
      //     'This app uses the photo library to keep photos made in app, add getting other images from library and attach them to the task.',
      // },
    },
    extra: {
      // TOKEN_DADATA: process.env.TOKEN_DADATA,
      PROTOCOL: process.env.PROTOCOL,
      HOST: process.env.HOST,
      // EXPERIENCE_ID: process.env.EXPERIENCE_ID,
    },
  }
}
