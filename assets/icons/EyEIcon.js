import React from 'react'
import Svg, { Path } from 'react-native-svg'

const EyEIcon = ({ color }) => {
  return (
    <Svg width="40" height="40" viewBox="0 0 40 40" fill="none" xmlns="http://www.w3.org/2000/svg">
      <Path
        d="M8.58111 13C6.49836 14.6355 4.52076 16.7431 2.72117 19.3227C2.58081 19.5261 2.5039 19.7665 2.50014 20.0136C2.49639 20.2607 2.56596 20.5034 2.70007 20.7109C6.4782 26.625 12.6407 31.25 19.9735 31.25C21.8927 31.25 23.7444 30.9252 25.5 30.3377M14 9.88118C15.9733 9.12706 17.9818 8.75 19.9735 8.75C27.172 8.75 33.5071 13.4594 37.3009 19.3188C37.4318 19.5223 37.5015 19.7592 37.5015 20.0012C37.5015 20.2432 37.4318 20.4801 37.3009 20.6836C35.7762 23.0711 33.8454 25.2495 31.6089 27"
        stroke={color}
        strokeWidth="1.4"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
      <Path
        d="M14 18.2442C13.8373 18.8012 13.75 19.3904 13.75 20C13.75 23.4518 16.5482 26.25 20 26.25C20.5171 26.25 21.0195 26.1872 21.5 26.0688M26.0688 21.5C26.1872 21.0195 26.25 20.5171 26.25 20C26.25 16.5482 23.4518 13.75 20 13.75C19.4829 13.75 18.9805 13.8128 18.5 13.9312"
        stroke={color}
        strokeWidth="1.4"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
      <Path
        d="M34 34L6 6"
        stroke={color}
        strokeWidth="1.4"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
    </Svg>
  )
}

export default EyEIcon
