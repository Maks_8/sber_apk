import * as React from "react"
import Svg, { Path } from "react-native-svg"

function Drag(props) {
    return (
        <Svg
            width={28}
            height={28}
            viewBox="0 0 28 28"
            fill="none"
            xmlns="http://www.w3.org/2000/svg"
            {...props}
        >
            <Path d="M4 10h20M4 14h20M4 18h20" stroke="silver" />
        </Svg>
    )
}

export default Drag
