import PasswordIcon from './passwordIcon'
import SharpIcon from './SharpIcon'
import SberLogo from './SberLogo'
import FaceIcon from './FaceIcon'
import EyEIcon from './EyEIcon'

export { PasswordIcon, SharpIcon, SberLogo, FaceIcon, EyEIcon }
