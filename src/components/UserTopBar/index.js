import React from 'react'
import { View, Text, TouchableOpacity, FlatList } from 'react-native'
import { useNavigation } from '@react-navigation/native'
import { showMessage } from '../../actions/messageActions'
import { colors, styles } from '../../styles'
import { useDispatch, useSelector } from 'react-redux'
import { Avatar } from 'react-native-paper'

import { SharpIcon, PasswordIcon } from '../../../assets/icons'

const UserTopBar = () => {
  const { user } = useSelector((state) => state.user)
  const navigation = useNavigation()

  return (
    <TouchableOpacity onPress={() => navigation.navigate('User', {})}>
      <View style={[styles.row, styles.ph20, styles.pt5, styles.pb16, styles.borderBottom]}>
        <View>
          <Text style={[styles.h2]}>{user.name}</Text>
          <Text style={(styles.text, styles.secondaryText)}>{user.prof}</Text>
        </View>
        <View style={[styles.relative]}>
          <Avatar.Image size={45} source={require('../../../assets/images/user.png')} />
          <View style={[styles.absolute, styles.pointNotification]}></View>
        </View>
      </View>
    </TouchableOpacity>
  )
}

export default UserTopBar
