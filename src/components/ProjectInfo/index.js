import React from 'react'
import { View, Text, TouchableOpacity } from 'react-native'
import { showMessage } from '../../actions/messageActions'
import { colors, styles } from '../../styles'
import { useDispatch, useSelector } from 'react-redux'
import { Button } from 'react-native-paper'
import ServiceInfo from '../ServiceInfo'
import ServiceInfoVM from '../ServiceInfoVM'
import ServiceInfoHD from '../ServiceInfoHD'
import { SharpIcon, EyEIcon } from '../../../assets/icons'
import { SwipeListView } from 'react-native-swipe-list-view'
import { useNavigation } from '@react-navigation/native'
import { addProjectToHiddenList, removeProjectFromHiddenList } from '../../actions/settingsActions'

const ProjectInfo = ({ projectdData }) => {
  const { projects } = useSelector((state) => state.projects)
  const { hidden_projects } = useSelector((state) => state.settings)
  const dispatch = useDispatch()
  const navigation = useNavigation()

  const handleHideProject = (id) => {
    dispatch(addProjectToHiddenList(id))
  }

  const handleShowProject = (id) => {
    dispatch(removeProjectFromHiddenList(id))
  }

  return (
    <>
      <SwipeListView
        data={projects}
        renderItem={(project) => {
          let isShown = true
          if (hidden_projects.includes(project.item.id)) {
            isShown = false
          }

          return (
            <View style={[!isShown && styles.mh8]}>
              <View
                style={[
                  styles.row,
                  styles.projectInfo,
                  styles.ph12,
                  styles.pv13,
                  isShown ? styles.backgroundPrimary : styles.hiddenProject,
                ]}
              >
                <Text style={[styles.buttonText]}>{project.item.title}</Text>
                {!isShown && (
                  <TouchableOpacity  onPress={() => handleShowProject(project.item.id)}>
                    <Text
                      style={[styles.buttonText, styles.buttonSmallText, styles.buttonTextLite]}
                    >
                      Отменить
                    </Text>
                  </TouchableOpacity>
                )}
              </View>
              {isShown && (
                <View style={[styles.servises]}>
                  {project.item.servises.map((service, idx) => {
                    if (idx < 3) {
                      if (service.type === 'VM') {
                        return <ServiceInfoVM key={service.id} service={service} />
                      } else if (service.type === 'HD') {
                        return <ServiceInfoHD key={service.id} service={service} />
                      } else {
                        return <ServiceInfo key={service.id} service={service} />
                      }
                    }
                  })}
                </View>
              )}
            </View>
          )
        }}
        renderHiddenItem={(project) => {
          let isShown = true
          if (hidden_projects.includes(project.item.id)) {
            isShown = false
          }

          return (
            isShown && (
              <View style={[styles.projectController]}>
                <View style={[styles.backRightBtn]}>
                  <TouchableOpacity
                    style={[styles.backRightBtnFirst, styles.pb16]}
                    onPress={() =>
                      navigation.navigate('ServiceSettings', { projectId: project.item.id })
                    }
                  >
                    <SharpIcon color={colors.text} />
                    <Text style={styles.span}>Service</Text>
                    <Text style={styles.span}>settings</Text>
                  </TouchableOpacity>
                  <TouchableOpacity
                    style={[styles.pt16]}
                    onPress={() => handleHideProject(project.item.id)}
                  >
                    <EyEIcon color={colors.text} />
                    <Text style={styles.span}>View off</Text>
                  </TouchableOpacity>
                </View>
              </View>
            )
          )
        }}
        disableRightSwipe
        rightOpenValue={-90}
        swipeRowStyle={styles.swipeRowStyle}
        ListFooterComponent={() => (
          <View style={[styles.row, styles.justifyCenter, styles.mb32, styles.pb32]}>
            <Button
              mode="contained"
              onPress={() => {
                navigation.navigate('DashSettings', {})
              }}
              compact
              style={[styles.buttonLite, styles.buttonRadius]}
              labelStyle={[
                styles.buttonText,
                styles.buttonSmallText,
                styles.buttonTextLite,
                styles.ph16,
                styles.pv5,
              ]}
              uppercase={false}
            >
              Dashboard Settings
            </Button>
          </View>
        )}
      />
    </>
  )
}

export default ProjectInfo
