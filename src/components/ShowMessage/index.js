import React from 'react'
import { Snackbar } from 'react-native-paper'

import { colors } from '../../styles'
import getStatusBarHeight from '../../utils/getStatusBarHeight'

const ShowMessage = ({ visible, setVisible, text, type }) => {
  const onDismissSnackBar = () => setVisible()
  let color = colors.primary

  switch (type) {
    case 'danger':
      color = colors.red
      break
    case 'success':
      color = colors.green
      break
    case 'warning':
      color = colors.yellow
      break
    default:
      color = colors.primary
  }

  return (
    <Snackbar
      visible={visible}
      onDismiss={onDismissSnackBar}
      duration={3000}
      style={{
        backgroundColor: color,
      }}
      wrapperStyle={{
        top: getStatusBarHeight(),
      }}
    >
      {text}
    </Snackbar>
  )
}

export default ShowMessage
