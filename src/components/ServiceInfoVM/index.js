import React from 'react'
import { View, Text, TouchableOpacity, FlatList } from 'react-native'
import { showMessage } from '../../actions/messageActions'
import { colors, styles } from '../../styles'
import { useDispatch, useSelector } from 'react-redux'
import { Button } from 'react-native-paper'

import { FaceIcon, PasswordIcon } from '../../../assets/icons'

const ServiceInfoVM = ({ service }) => {
  const { title, params } = service
  const dispatch = useDispatch()

  console.log('params', params)

  return (
    <View style={[styles.row, styles.ph12, styles.pv12, styles.borderBottom]}>
      <View style={[styles.row]}>
        <FaceIcon color={colors.text} />
        <Text style={[styles.text, styles.pv12, styles.textUppercase, styles.pl8]}>{title}</Text>
      </View>
      <View>
        <Text style={[styles.span, styles.secondaryText]}>servers</Text>
        <Text style={[styles.text]}>{params[0].count}</Text>
      </View>
    </View>
  )
}

export default ServiceInfoVM
