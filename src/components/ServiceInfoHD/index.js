import React from 'react'
import { View, Text, TouchableOpacity, FlatList } from 'react-native'
import { showMessage } from '../../actions/messageActions'
import { colors, styles } from '../../styles'
import { useDispatch, useSelector } from 'react-redux'
import { Button } from 'react-native-paper'

import { FaceIcon, PasswordIcon } from '../../../assets/icons'

const ServiceInfoHD = ({ service }) => {
  const { title, params } = service
  const dispatch = useDispatch()
  let width = (params[0].current / params[0].max) * 100 + '%'
  let color = params[0].overload ? colors.red : colors.primary

  return (
    <View style={[styles.row, styles.ph12, styles.pv12, styles.borderBottom]}>
      <View style={[styles.row, styles.flex2, styles.justifyStart]}>
        <FaceIcon color={colors.text} />
        <Text style={[styles.text, styles.pv12, styles.textUppercase, styles.pl8]}>{title}</Text>
        <View
          style={{
            width: width,
            position: 'absolute',
            top: 0,
            left: -12,
            backgroundColor: color,
            height: '100%',
            opacity: 0.1,
          }}
        ></View>
      </View>
      <View style={[styles.flex1, styles.borderLeft, styles.positionEnd]}>
        <Text style={[styles.span, styles.secondaryText]}>Disk Usage</Text>
        <Text style={[styles.hdParams, styles.text, params[0].overload && styles.backgroundOrange]}>
          {params[0].current}/{params[0].max}
        </Text>
      </View>
    </View>
  )
}

export default ServiceInfoHD
