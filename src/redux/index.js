import { applyMiddleware, createStore, compose } from 'redux'
// import thunkMiddleware from 'redux-thunk'
import { createLogger } from 'redux-logger'
import rootReducer from '../reducers/rootReducer'

const enhancers = [
  applyMiddleware(
    // thunkMiddleware
    createLogger({
      collapsed: true,
      predicate: () => __DEV__,
    })
  ),
]

const composeEnhancers =
  (__DEV__ && typeof window !== 'undefined' && window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__) ||
  compose

const enhancer = composeEnhancers(...enhancers)

export const store = createStore(rootReducer, enhancer)
