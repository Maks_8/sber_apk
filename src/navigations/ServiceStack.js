import React from 'react'
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs'

import MainStackView from './MainStack'
import SettingsStackView from './SettingsStack'
// import { navigationOptions } from '../styles'

const Tab = createBottomTabNavigator()

const ServiceStack = () => {
  return (
    <Tab.Navigator>
      <Tab.Screen name="Main" component={MainStackView} />
      <Tab.Screen name="Settings" component={SettingsStackView} />
    </Tab.Navigator>
  )
}

export default ServiceStack
