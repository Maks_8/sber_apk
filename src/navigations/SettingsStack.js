import React from 'react'
import { createStackNavigator } from '@react-navigation/stack'

import { MainScreen } from '../view'
import { navigationOptions } from '../styles'

const SettingsStack = createStackNavigator()

const SettingsStackView = () => {
  return (
    <SettingsStack.Navigator initialRouteName="Settings" headerMode="screen">
      <SettingsStack.Screen
        name="Settings"
        component={MainScreen}
        options={{ ...navigationOptions, headerShown: false, title: 'MainScreen' }}
      />
    </SettingsStack.Navigator>
  )
}

export default SettingsStackView
