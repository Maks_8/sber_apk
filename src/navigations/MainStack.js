import React from 'react'
import { createStackNavigator } from '@react-navigation/stack'
import { MainScreen, ServiceSettingsScreen, DashSettingsScreen, UserScreen } from '../view'
import { navigationOptions } from '../styles'

const MainStack = createStackNavigator()

const MainStackView = () => {
  return (
    <MainStack.Navigator initialRouteName="Main" headerMode="screen">
      <MainStack.Screen
        name="Main"
        component={MainScreen}
        options={{ ...navigationOptions, headerShown: false, title: 'MainScreen' }}
      />
      <MainStack.Screen
        name="DashSettings"
        component={DashSettingsScreen}
        options={{ ...navigationOptions, headerShown: false, title: 'DashSettingsScreen' }}
      />
      <MainStack.Screen
        name="ServiceSettings"
        component={ServiceSettingsScreen}
        options={{ ...navigationOptions, headerShown: false, title: 'ServiceSettingsScreen' }}
      />
      <MainStack.Screen
        name="User"
        component={UserScreen}
        options={{ ...navigationOptions, headerShown: false, title: 'UserScreen' }}
      />
    </MainStack.Navigator>
  )
}

export default MainStackView
