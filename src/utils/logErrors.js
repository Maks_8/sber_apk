function logErrors(view, type) {
  console.log(`Error on view => ${view}; Type of error => ${type}`)
}

export default logErrors
