import logErrors from './logErrors'
import getPushToken from './getPushToken'
import AsyncStorage from '@react-native-async-storage/async-storage'
import { sendToken } from '../api/api'

const sendNotificationToken = async (userID = '') => {
  try {
    const pushNotificationConfirm = await AsyncStorage.getItem('notification')
    // if (pushNotificationConfirm === null) {
    const PushTokenData = await getPushToken()
    if (PushTokenData) {
      PushTokenData.user_id = userID
    }
    if (PushTokenData.token) {
      const result = await sendToken(JSON.stringify(PushTokenData))
      // if ('id' in result.result) {
      //   await AsyncStorage.setItem('notification', JSON.stringify(Date.now()))
      // }
    } else {
      logErrors('sendNotificationToken', `no token in PushTokenData`)
    }
    // }
  } catch (err) {
    logErrors(
      'sendNotificationToken',
      `pushNotificationConfirm AsyncStorage getItem for notification get err ${err}`
    )
  }
}

export default sendNotificationToken
