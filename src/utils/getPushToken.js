import { Platform } from 'react-native'
import Constants from 'expo-constants'
import * as Permissions from 'expo-permissions'
import * as Notifications from 'expo-notifications'
import logErrors from './logErrors'

async function getStatus() {
  try {
    const { status: existingStatus } = await Permissions.getAsync(Permissions.NOTIFICATIONS)
    let finalStatus = existingStatus
    if (existingStatus !== 'granted') {
      const { status } = await Permissions.askAsync(Permissions.NOTIFICATIONS)
      finalStatus = status
    }
    if (finalStatus !== 'granted') {
      logErrors('getStatus', `Permissions Уведомления отключены`)
      return
    }
    return finalStatus
  } catch (e) {
    logErrors('getStatus', `Permissions getAsync for Permissions.NOTIFICATIONS get err ${e}`)
  }
}

async function getPushToken() {
  if (Constants.isDevice) {
    const finalStatus = await getStatus()

    if (finalStatus !== 'granted') {
      return
    }

    try {
      const experienceId = Constants.manifest.extra.EXPERIENCE_ID
      const token = (await Notifications.getExpoPushTokenAsync({ experienceId })).data
      const deviceToken = (await Notifications.getDevicePushTokenAsync()).data
      return {
        token,
        device: Platform.OS,
        user_id: '',
        device_token: deviceToken,
      }
    } catch (e) {
      logErrors('token', `getExpoPushTokenAsync get err ${e}`)
    }
  } else {
    alert('Notifications доступны только на реальных устройствах')
  }

  return {
    token: '',
    device_token: '',
    user_id: '',
    device: Platform.OS,
  }
}

export default getPushToken
