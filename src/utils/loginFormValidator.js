export const getErrorData = (fieldType, value) => {
    if(fieldType === 'login'){
        if(value === ''){
            return 'Login cannot be empty'
        }
        if(value.match(/[а-яё]+$/iug)){
            return 'login can contain only Latin letters'
        }
        return ''
    }
    if(fieldType === 'password'){
        if(value === ''){
            return 'Password cannot be empty'
        }
        if(value.match(/[а-яё]+$/iug)){
            return 'Password can contain only Latin letters'
        }
        if(value.length < 6){
            return 'Password min length 6 symbols'
        }
        if(!value.match(/^($|.*[0-9])/)){
            return 'Password can contain one number'
        }
        return ''
    }
}
