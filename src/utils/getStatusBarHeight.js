import { Platform, StatusBar } from 'react-native'
import { getStatusBarHeight as getStatusBarHeightIOS } from 'react-native-status-bar-height'

const getStatusBarHeight = () => {
  if (Platform.OS === 'ios') {
    return getStatusBarHeightIOS()
  }

  return StatusBar.currentHeight
}

export default getStatusBarHeight
