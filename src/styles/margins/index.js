const margins = {
  /* Top */
  mt5: {
    marginTop: 5,
  },
  mt20: {
    marginTop: 20,
  },
  mt40: {
    marginTop: 40,
  },
  mt80: {
    marginTop: 80,
  },
  /* Bottom */
  mb0: {
    marginBottom: 0,
  },
  mb5: {
    marginBottom: 5,
  },
  mb8:{
    marginBottom: 8
  },
  mb10: {
    marginBottom: 10,
  },
  mb12: {
    marginBottom: 12,
  },
  mb15: {
    marginBottom: 15,
  },
  mb20: {
    marginBottom: 10,
  },
  mb28: {
    marginBottom: 28,
  },
  mb32: {
    marginBottom: 32,
  },
  mb40: {
    marginBottom: 40,
  },
  mb60: {
    marginBottom: 60,
  },
  /* Horizontal */
  mh8: {
    marginHorizontal: 8,
  },

  mr12: {
    marginRight: 12
  }
}

export default margins
