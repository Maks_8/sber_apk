const colors = {
  white: '#ffffff',
  primary: '#07E897',
  accent: '#f1c40f',
  placeholder: '#727272',
  text: '#343F48',
  secondaryText: '#D2D2D2',
  grey: '#E0E0E0',
  grey100: '#9E9E9E',
  liteGreen: '#E6FAF3',
  black: '#000',
  line: '#EEEEEE',

  red: '#FF4F49',
  green: '#2F8642',
  yellow: '#f0ad4e',
  orange: '#B7521A',
}

export default colors
