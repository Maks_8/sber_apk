import React from 'react'
import getStatusBarHeight from '../utils/getStatusBarHeight'
import { Platform } from 'react-native'

// Template
import colors from './colors'
// import { HeaderBackIcon } from '../../assets/icons'

const height = getStatusBarHeight()

const navigationOptions = {
  headerStyle: {
    borderTopWidth: 0.5,
    borderTopColor: colors.grey,
    elevation: 0,
    shadowOpacity: 0,
    height: height * 2.5,
  },
  // headerBackImage: () => {
  //   return <HeaderBackIcon color={colors.placeholder} />
  // },
  headerBackTitleVisible: true,
  headerBackTitleStyle: {
    color: colors.placeholder,
    fontSize: 14,
    fontStyle: 'normal',
    lineHeight: 24,
    paddingLeft: 10,
  },
  headerTitleContainerStyle: {
    paddingHorizontal: 20,
  },
  headerLeftContainerStyle: {
    width: '50%',
    paddingLeft: Platform.OS === 'ios' ? 8 : 4,
    // paddingTop: 6,
    // paddingBottom: 10,
    borderTopWidth: 0.5,
    borderTopColor: colors.grey,
    alignItems: 'center',
  },
  headerRightContainerStyle: {
    width: '50%',
    paddingTop: 16,
    borderTopWidth: 0.5,
    borderTopColor: colors.grey,
    alignItems: 'center',
  },
  headerPressColorAndroid: colors.white,
}

export default navigationOptions
