import colors from './colors'
import paddings from './paddings'
import margins from './margins'

const layout = {
  container: {
    flex: 1,
    backgroundColor: colors.white,
  },
  flex1: {
    flex: 1,
  },
  flex2: {
    flex: 2,
  },
  justifyBetween: {
    justifyContent: 'space-between',
  },
  positionEnd: {
    alignItems: 'flex-end',
  },
  positionStart: {
    alignItems: 'flex-start',
  },
  row: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  passwordCont: {
    borderBottomWidth: 1,
    borderColor: colors.text,
  },
  activeBorder: {
    borderColor: colors.primary,
  },
  relative: {
    position: 'relative',
  },
  absolute: {
    position: 'absolute',
  },
  errorBorder: {
    borderColor: colors.red,
  },
  projectIcon: {
    flex: 0,
  },
  justifyEnd: {
    justifyContent: 'flex-end',
  },
  justifyStart: {
    justifyContent: 'flex-start',
  },
  justifyCenter: {
    justifyContent: 'center',
  },
  borderBottom: {
    borderBottomWidth: 1,
    borderBottomColor: colors.line,
  },
  borderLeft: {
    borderLeftWidth: 1,
    borderLeftColor: colors.line,
  },
  pointNotification: {
    bottom: 0,
    right: 0,
    width: 10,
    height: 10,
    borderRadius: 10,
    borderWidth: 1,
    borderColor: colors.white,
    backgroundColor: colors.primary,
  },
  backgroundPrimary: {
    backgroundColor: colors.primary,
  },
  backgroundSecondaryText: {
    backgroundColor: colors.secondaryText,
  },
  backgroundOrange: {
    backgroundColor: colors.red,
  },
  backgroundText: {
    backgroundColor: colors.text,
  },
  backgroundWhite: {
    backgroundColor: colors.white,
  },
  projectInfo: {
    alignItems: 'flex-start',
    width: '100%',
    borderTopLeftRadius: 8,
    borderTopRightRadius: 8,
  },
  projectController: {
    alignItems: 'center',
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
    // paddingLeft: 15,
  },
  servises: {
    backgroundColor: colors.white,
    borderBottomLeftRadius: 8,
    borderBottomRightRadius: 8,
  },
  swipeRowStyle: {
    marginHorizontal: 8,
    borderRadius: 8,
    marginBottom: 20,
    backgroundColor: colors.secondaryText,
  },
  backRightBtn: {
    alignItems: 'center',
    bottom: 0,
    justifyContent: 'center',
    position: 'absolute',
    top: 0,
    width: 80,
    right: 0,
  },
  backRightBtnFirst: {
    borderBottomWidth: 1,
    borderBottomColor: colors.white,
  },
  hiddenProject: {
    backgroundColor: colors.text,
    borderBottomLeftRadius: 8,
    borderBottomRightRadius: 8,
    marginBottom: 20,
  },
  hdParams: {
    paddingHorizontal: 4,
    paddingVertical: 1,
    borderRadius: 4,
  },
  bgT: {
    backgroundColor: 'rgba(255,255,255, 0.7)',
    shadowColor: colors.black,
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 0.2,
    shadowRadius: 2,
    elevation: 2,
  },
  ...paddings,
  ...margins,
  h48:{
    height: 48
  },
  bottomDash:{
    height: 64,
    borderTopWidth: 1,
    borderColor: colors.secondaryText
  },

  w171:{
    width: 171
  }
}

export default layout
