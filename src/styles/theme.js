import { DefaultTheme } from 'react-native-paper'
import colors from './colors'

const theme = {
  ...DefaultTheme,
  roundness: 2,
  colors: {
    ...DefaultTheme.colors,
    primary: colors.primary,
    accent: colors.accent,
    background: colors.white,
    placeholder: colors.placeholder,
    text: colors.text,
    disabled: colors.grey100,
  },
}

export default theme
