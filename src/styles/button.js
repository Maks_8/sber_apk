import colors from './colors'

const button = {
  button: {
    width: '100%',
    justifyContent: 'center',
    alignItems: 'center',
    height: 48,
    borderRadius: 12,
    backgroundColor: colors.primary,
    shadowColor: colors.black,
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 0.22,
    shadowRadius: 2.22,
    elevation: 3,
  },

  buttonLite: {
    backgroundColor: colors.liteGreen,
    shadowColor: colors.black,
    shadowOffset: {
      width: 0,
      height: 0,
    },
    shadowOpacity: 0,
    shadowRadius: 0,

    elevation: 0,
  },
  buttonRadius: {
    borderRadius: 12,
  },

  whiteBtn:{
    backgroundColor: colors.white,
    shadowColor: colors.black,
    shadowOffset: {
      width: 0,
      height: 0,
    },
    shadowOpacity: 0,
    shadowRadius: 0,

    elevation: 0,
  }

}
export default button
