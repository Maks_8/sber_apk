import colors from './colors'

const input = {
    input: {
        width: '100%',
        height: 24,
        borderBottomWidth: 1,
        borderColor: colors.text,
        color: colors.text,
        fontSize: 16,
        lineHeight: 20,
        fontWeight: 'bold',
        fontFamily: 'SBSansBold'
    },

    placeholder: {
        fontSize: 12,
        fontFamily: 'SBSansRegular',
        lineHeight: 16,
        fontWeight: '400',
    },

    password: {
        flex: 1,
        borderBottomWidth: 0
    }
}
export default input
