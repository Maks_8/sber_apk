const paddings = {
  /* Horizontal */
  ph8: {
    paddingHorizontal: 8,
  },
  ph10: {
    paddingHorizontal: 10,
  },
  ph12: {
    paddingHorizontal: 12,
  },
  ph16: {
    paddingHorizontal: 16,
  },
  ph20: {
    paddingHorizontal: 20,
  },
  ph40: {
    paddingHorizontal: 40,
  },
  /* Top */
  pt5: {
    paddingTop: 5,
  },
  pt16: {
    paddingTop: 16,
  },
  pt20: {
    paddingTop: 20,
  },
  /* Bottom */
  pb16: {
    paddingBottom: 16,
  },
  pb32: {
    paddingBottom: 32,
  },
  /* Vertical */
  pv5: {
    paddingVertical: 5,
  },
  pv10: {
    paddingVertical: 10,
  },
  pv12: {
    paddingVertical: 13,
  },
  pv13: {
    paddingVertical: 13,
  },
  pv24: {
    paddingVertical: 24,
  },
  /* */
  pl8: {
    paddingLeft: 8,
  },
}

export default paddings
