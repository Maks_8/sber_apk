import colors from './colors'

const text = {
  h1: {
    fontFamily: 'SBSansRegular',
    fontSize: 24,
    fontStyle: 'normal',
    lineHeight: 24,
    letterSpacing: 0,
    color: colors.text,
  },
  h2: {
    fontFamily: 'SBSansRegular',
    fontSize: 20,
    fontStyle: 'normal',
    fontWeight: 'bold',
    lineHeight: 24,
    letterSpacing: 0,
    color: colors.text,
  },
  textCenter: {
    textAlign: 'center',
  },

  text: {
    fontFamily: 'SBSansRegular',
    color: colors.text,
    fontSize: 14,
    lineHeight: 18,
    textAlign: 'center',
  },

  textError: {
    fontFamily: 'SBSansRegular',
    color: colors.red,
    fontWeight: '400',
    fontSize: 12,
    lineHeight: 16,
  },

  labelInput: {
    fontFamily: 'SBSansRegular',
    color: colors.text,
    fontWeight: '400',
    fontSize: 12,
    lineHeight: 16,
  },

  forgotText: {
    fontFamily: 'SBSansBold',
    fontWeight: 'bold',
    fontSize: 14,
    lineHeight: 18,
    color: colors.secondaryText,
  },

  title:{
    fontFamily: 'SBSansBold',
    fontWeight: 'bold',
    fontSize: 24,
    lineHeight: 32,
    color: colors.text
  },

  activeLabel: {
    color: colors.primary,
  },
  span: {
    fontFamily: 'SBSansRegular',
    fontSize: 10,
    lineHeight: 13,
    textAlign: 'center',
    textTransform: 'uppercase',
    color: colors.text,
  },
  buttonText: {
    fontFamily: 'SBSansBold',
    fontWeight: 'bold',
    fontSize: 14,
    lineHeight: 18,
    color: colors.white,
  },

  dragText: {
    fontFamily: 'SBSansRegular',
    fontSize: 14,
    lineHeight: 16,
    color: colors.text,
  },

  buttonSmallText: {
    fontSize: 12,
    lineHeight: 16,
  },

  mediumSmallText: {
    fontFamily: 'SBSansRegular',
    fontSize: 12,
    lineHeight: 16,
    color: colors.text
  },
  buttonTextLite: {
    color: colors.primary,
  },
  secondaryText: {
    color: colors.secondaryText,
  },
  whiteText: {
    color: colors.white,
  },
  textUppercase: {
    textTransform: 'uppercase',
  },
}

export default text
