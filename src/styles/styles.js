import { StyleSheet } from 'react-native'
import layout from './layout'
import text from './text'
import input from './input'
import button from "./button";

const styles = StyleSheet.create({
  ...layout,
  ...text,
  ...input,
  ...button,
})

export default styles
