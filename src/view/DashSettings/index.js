import React, {useCallback, useEffect, useState} from 'react'
import { View, Text, TouchableOpacity, ScrollView, ListItem } from 'react-native'
import { showMessage } from '../../actions/messageActions'
import { colors, styles } from '../../styles'
import { useDispatch, useSelector } from 'react-redux'
import {Button, Switch} from 'react-native-paper'
import ProjectInfo from '../../components/ProjectInfo'
import {SafeAreaView} from "react-native-safe-area-context";
import DraggableFlatList, { RenderItemInfo, OnMoveEndInfo } from 'react-native-draggable-flatlist';
import Drag from "../../../assets/icons/drag";
import {addProjectToHiddenList, removeProjectFromHiddenList} from "../../actions/settingsActions";

const DashSettings = ({ navigation }) => {
    const dispatch = useDispatch()
    const { projects } = useSelector((state) => state.projects)
    const { hidden_projects } = useSelector((state) => state.settings)
    const [data, setData] = useState([])
    const [hiddenData, setHiddenData] = useState([])

    const handleHideProject = (id) => {
        dispatch(addProjectToHiddenList(id))
    }

    const handleShowProject = (id) => {
        dispatch(removeProjectFromHiddenList(id))
    }

    const onToggleSwitch = (id) => {
        if(hidden_projects.find(x => x === id)){
            handleShowProject(id)
        }
        else{
            handleHideProject(id)
        }
    }



    useEffect(() => {
        const show = [];
        const hidden = [];
        projects.forEach(item => {console.log(hidden_projects)
            if(hidden_projects.length > 0 && hidden_projects.find(x => x === item.id)){
            hidden.push(item)
        } else {
            show.push(item)
        }
        })
        setData([...show])
        setHiddenData([...hidden])
    }, [hidden_projects])

    const renderItem = useCallback(
        ({ item, index, drag, isActive }) => {
            return (
                <View style={[styles.row, styles.h48, styles.borderBottom, isActive && styles.bgT]}>
                    <Switch style={[styles.mr12, ]} color={colors.primary} value={true} onValueChange={() => onToggleSwitch(item.id)} />
                    <TouchableOpacity style={[styles.flex1, styles.positionStart]}
                                      onPress={() =>
                                          navigation.navigate('ServiceSettings', { projectId: item.id })
                                      }>
                        <Text style={[styles.dragText]} >{item.title}</Text>
                    </TouchableOpacity>
                    <TouchableOpacity
                        key={index}
                        onLongPress={drag}>
                        <Drag />
                    </TouchableOpacity>
                </View>
            );
        },
        []
    );

  return (
      <SafeAreaView style={[styles.container, styles.justifyBetween]}>
          <View style={[styles.ph8]} >
              <Text style={[styles.title, styles.mb8, styles.mt40]} >Dashboard Settings</Text>
              <Text style={[styles.mediumSmallText, styles.mb40]} >Получайте инфу из необходимых систем оперативно. Включайте/отключайте видимость и организуйте доступные проекты(?)</Text>
              <Text style={[styles.mediumSmallText, styles.secondaryText, styles.mb12]}>View on</Text>
              <View style={{height: 180}}>
                  <DraggableFlatList
                      data={data}
                      renderItem={renderItem}
                      keyExtractor={(item, index) => `draggable-item-${item.id}`}
                      onDragEnd={({data}) => setData(data)}
                  />
              </View>
              <Text style={[styles.mediumSmallText, styles.secondaryText, styles.mb12]} >View off</Text>
              <View>
                  {hiddenData.map(item => {
                      return (
                          <View key={item.id} style={[styles.row, styles.h48, styles.borderBottom]}>
                              <Switch style={[styles.mr12, ]} value={false} onValueChange={() => onToggleSwitch(item.id)} />
                              <View style={[styles.flex1, styles.positionStart]}>
                                  <Text style={[styles.dragText]} >{item.title}</Text>
                              </View>
                          </View>
                      )
                  })}
              </View>
          </View>
          <View style={[styles.row, styles.bottomDash, styles.ph12]}>
              <TouchableOpacity onPress={() => {
                  navigation.navigate('Main', {})
              }} style={[styles.button, styles.w171, styles.whiteBtn]}><Text style={[styles.buttonText, styles.secondaryText]}>Cancel</Text></TouchableOpacity>
              <TouchableOpacity onPress={() => {
                  navigation.navigate('Main', {})
              }} style={[styles.button, styles.w171]}><Text style={styles.buttonText}>Done</Text></TouchableOpacity>
          </View>
      </SafeAreaView>
  )
}

export default DashSettings
