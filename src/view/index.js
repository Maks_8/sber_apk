import AuthorizationScreen from './Authorization'
import MainScreen from './Main'
import ServiceSettingsScreen from './ServiceSettings'
import DashSettingsScreen from './DashSettings'
import UserScreen from './User'

export { MainScreen, AuthorizationScreen, ServiceSettingsScreen, DashSettingsScreen, UserScreen }
