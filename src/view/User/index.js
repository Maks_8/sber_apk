import React from 'react'
import { View, Text, ScrollView } from 'react-native'
import { useNavigation } from '@react-navigation/native'
import { SafeAreaView } from 'react-native-safe-area-context'
import { Button } from 'react-native-paper'
import UserTopBar from '../../components/UserTopBar'
import { colors, styles } from '../../styles'

const User = () => {
  const navigation = useNavigation()

  return (
    <SafeAreaView style={[styles.container]}>
      <UserTopBar />
      <ScrollView style={[styles.mt20, styles.ph16]}>
        <View style={[styles.mb12]}>
          <Text style={[styles.h2, styles.mb12]}>Сообщения: </Text>
          <Text>Сообщение 1</Text>
          <Text>Сообщение 2</Text>
          <Text>Сообщение 3</Text>
        </View>
        <View style={[styles.row, styles.mb12]}>
          <Text style={[styles.h2]}>Оганизация</Text>
          <Text>ПАО "Сбербанк"</Text>
        </View>
        <View style={[styles.row, styles.mb12]}>
          <Text style={[styles.h2]}>Отдел</Text>
          <Text>Разработка SberCloud</Text>
        </View>
      </ScrollView>
      <View style={[styles.row, styles.justifyCenter, styles.mb32]}>
        <Button
          mode="contained"
          onPress={() => {
            navigation.navigate('Main', {})
          }}
          compact
          style={[styles.buttonLite, styles.buttonRadius]}
          labelStyle={[
            styles.buttonText,
            styles.buttonSmallText,
            styles.buttonTextLite,
            styles.ph16,
            styles.pv5,
          ]}
          uppercase={false}
        >
          Dashboard
        </Button>
      </View>
    </SafeAreaView>
  )
}

export default User
