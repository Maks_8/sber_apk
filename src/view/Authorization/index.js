import React, { useEffect, useState } from 'react'
import {
  View,
  TextInput,
  TouchableOpacity,
  Text,
  StyleSheet,
  Keyboard,
  KeyboardAvoidingView,
} from 'react-native'
import { styles, colors } from '../../styles'
import { PasswordIcon } from '../../../assets/icons'
import { Authorization } from '../../api/api'
import { useDispatch, useSelector } from 'react-redux'
import { userLogInAction } from '../../actions/userActions'
import SberLogo from '../../../assets/icons/SberLogo'
import { SafeAreaView } from 'react-native-safe-area-context'
import { getErrorData } from '../../utils/loginFormValidator'
import { showMessage } from '../../actions/messageActions'

export default function LoginPage() {
  const dispatch = useDispatch()

  const [hidePassword, setHidePassword] = useState(true)

  //secureTextEntry стирает текст на IOS особенности поведения платформы
  const [passwordValue, setPasswordValue] = useState('')

  const [loginValue, setLoginValue] = useState('')

  //дублирование кода для отображения надо перенести инпут в компонет
  const [activeEnterLogin, setActiveEnterLogin] = useState(false)
  const [activeEnterPassword, setActiveEnterPassword] = useState(false)

  const [loginError, setLoginError] = useState('')
  const [passwordError, setPasswordError] = useState('')

  const sendAuth = async (loginValue, passwordValue) => {
    setLoginError(getErrorData('login', loginValue))
    setPasswordError(getErrorData('password', passwordValue))
    if (
      getErrorData('login', loginValue) === '' &&
      getErrorData('password', passwordValue) === ''
    ) {
      const result = await Authorization(loginValue, passwordValue)
      if (result === 'success') {
        dispatch(userLogInAction({ name: 'Kirill Shekhovtsov', prof: 'Senior Developer' }))
      } else {
        dispatch(showMessage('an unhandled exception occurred', 'danger'))
      }
    }
  }

  return (
    <SafeAreaView
      onPress={() => Keyboard.dismiss()}
      style={[styles.container, styles.justifyBetween]}
      onStartShouldSetResponder={() => Keyboard.dismiss()}
    >
      <View style={[styles.ph40, styles.mt80]}>
        <SberLogo />
      </View>
      <View style={styles.ph40}>
        <Text style={[styles.labelInput, styles.mb10, activeEnterLogin && styles.activeLabel]}>
          Account name or email
        </Text>
        <TextInput
          value={loginValue}
          onFocus={() => setActiveEnterLogin(true)}
          onBlur={() => setActiveEnterLogin(false)}
          style={[
            styles.input,
            loginValue === '' && styles.placeholder,
            loginError !== '' && styles.errorBorder,
            activeEnterLogin && styles.activeBorder,
          ]}
          onChangeText={(text) => setLoginValue(text)}
          placeholder="Write account name or email"
          placeholderTextColor={colors.secondaryText}
        />
        <Text style={[styles.textError, styles.mt5, styles.mb40]}>{loginError}</Text>
        <Text style={[styles.labelInput, styles.mb10, activeEnterPassword && styles.activeLabel]}>
          Password
        </Text>
        <View
          style={[
            styles.row,
            styles.passwordCont,
            passwordError !== '' && styles.errorBorder,
            activeEnterPassword && styles.activeBorder,
          ]}
        >
          <TextInput
            value={passwordValue}
            onFocus={() => setActiveEnterPassword(true)}
            onBlur={() => setActiveEnterPassword(false)}
            style={[styles.input, styles.password, passwordValue === '' && styles.placeholder]}
            secureTextEntry={hidePassword}
            placeholder="Write password"
            onChangeText={(text) => setPasswordValue(text)}
            placeholderTextColor={colors.secondaryText}
          />
          <TouchableOpacity style={styles.mb5} onPress={() => setHidePassword(!hidePassword)}>
            <PasswordIcon />
          </TouchableOpacity>
        </View>
        <Text style={[styles.textError, styles.mt5, styles.mb20]}>{passwordError}</Text>

        <View style={styles.positionEnd}>
          <TouchableOpacity
            onPress={() => dispatch(showMessage('Функционал не реализован', 'warning'))}
          >
            <Text style={styles.forgotText}>I forgot password</Text>
          </TouchableOpacity>
        </View>
      </View>
      <View style={[styles.mb60, styles.ph40]}>
        <TouchableOpacity
          onPress={() => sendAuth(loginValue, passwordValue)}
          style={[styles.button, styles.mb28]}
        >
          <Text style={styles.buttonText}>Log in</Text>
        </TouchableOpacity>
        <Text style={[styles.mb12, styles.text]}>or continue with</Text>
        <TouchableOpacity
          style={[styles.button, styles.buttonLite]}
          onPress={() => dispatch(showMessage('Функционал не реализован', 'warning'))}
        >
          <Text style={[styles.buttonText, styles.buttonTextLite]}>Sber ID</Text>
        </TouchableOpacity>
      </View>
    </SafeAreaView>
  )
}
