import React, { useEffect, useState } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { NavigationContainer } from '@react-navigation/native'
import { createStackNavigator } from '@react-navigation/stack'
import { Provider as PaperProvider } from 'react-native-paper'
// import * as SecureStore from 'expo-secure-store'
import NetInfo from '@react-native-community/netinfo'

import { AuthorizationScreen } from './index'
import ShowMessage from '../components/ShowMessage'
// import req from '../utils/request'
// import logErrors from '../utils/logErrors'
import { hideMessage, showMessage } from '../actions/messageActions'
// import { userUpdateInfoAction, userLogInAction } from '../actions/userActions'
import { setNetInfo } from '../actions/appActions'
import { theme, navigationOptions } from '../styles'
import MainStackView from '../navigations/MainStack'
import { SafeAreaProvider } from 'react-native-safe-area-context'
import {userLogInAction} from "../actions/userActions";

const Stack = createStackNavigator()

const AppView = () => {
  const [isLoading, setIsLoading] = useState(false)
  const controller = new AbortController()
  const dispatch = useDispatch()
  const message = useSelector((state) => state.message)
  const user = useSelector((state) => state.user)
  const { isConnected } = useSelector((state) => state.app)

  useEffect(() => {
    NetInfo.addEventListener((state) => {
      if (!state.isConnected) {
        dispatch(showMessage('Нет доступа к сети', 'warning'))
      }
      dispatch(setNetInfo(state.isConnected))
    })

    //return unsubscribe()
  }, [])

  useEffect(() => {
    // login action
    dispatch(userLogInAction({ name: 'Kirill Shekhovtsov', prof: 'Senior Developer' }))
  }, [user.login])

  if (isLoading) {
    return null
  }

  return (
    <PaperProvider theme={theme}>
      <SafeAreaProvider>
        <NavigationContainer>
          <Stack.Navigator initialRouteName="Authorization" headerMode="screen">
            {user.login ? (
              <Stack.Screen
                name="Main"
                component={MainStackView}
                options={{ ...navigationOptions, headerShown: false }}
              />
            ) : (
              <Stack.Screen
                name="Authorization"
                component={AuthorizationScreen}
                options={{ ...navigationOptions, headerShown: false }}
              />
            )}
          </Stack.Navigator>
        </NavigationContainer>
      </SafeAreaProvider>
      <ShowMessage
        visible={message.show}
        setVisible={() => dispatch(hideMessage())}
        text={message.text}
        type={message.messageType}
      />
    </PaperProvider>
  )
}

export default AppView
