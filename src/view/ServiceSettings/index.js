import React, {useEffect, useState} from 'react'
import { View, Text } from 'react-native'
import { SafeAreaView } from 'react-native-safe-area-context'

import { colors, styles } from '../../styles'
import {useDispatch, useSelector} from "react-redux";

const ServiceSettings = (projectId) => {
    const dispatch = useDispatch()
    const { projects } = useSelector((state) => state.projects)
    const [project, setProject] = useState({})

    // useEffect(() => {
    //     console.log(projectId)
    //     if(projects.find(x => x.id === id)){
    //         setProject(projects.find(x => x.id === id))
    //     }
    // }, [])


    return (
        <SafeAreaView style={[styles.container, styles.justifyBetween]}>
            <View style={[styles.mt20, styles.mb40, styles.ph16]}>
                <Text style={[styles.title, styles.mb8, styles.mt40]} >Settings</Text>
            </View>
        </SafeAreaView>
     )
}

export default ServiceSettings
