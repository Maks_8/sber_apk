import React, { useEffect } from 'react'
import { View, Text, TouchableOpacity, ScrollView } from 'react-native'
import { SafeAreaView } from 'react-native-safe-area-context'
import { showMessage } from '../../actions/messageActions'
import { colors, styles } from '../../styles'
import { useDispatch, useSelector } from 'react-redux'
import { Button } from 'react-native-paper'
import ProjectInfo from '../../components/ProjectInfo'
import UserTopBar from '../../components/UserTopBar'
import sendNotificationToken from '../../utils/sendNotificationToken'
import { SwipeListView } from 'react-native-swipe-list-view'

const Main = ({ navigation }) => {
  const dispatch = useDispatch()

  useEffect(() => {
    sendNotificationToken()
  }, [])

  const projectdData = {
    title: 'Риваль',
    servises: [
      {
        id: 1,
        title: 'Cloud Eye',
        params: [
          {
            name: 'some',
            value: 10,
          },
        ],
      },
    ],
  }

  return (
    <SafeAreaView style={[styles.container]}>
      <UserTopBar />
      <View style={[styles.mt20, styles.mb40]}>
        <ProjectInfo />
      </View>
    </SafeAreaView>
  )
}

export default Main
