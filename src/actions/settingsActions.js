import { ADD_PROJECT_HIDDEN_LIST, REMOVE_PROJECT_HIDDEN_LIST } from './index'

export const addProjectToHiddenList = (projectID) => {
  return {
    type: ADD_PROJECT_HIDDEN_LIST,
    projectID,
  }
}

export const removeProjectFromHiddenList = (projectID) => {
  return {
    type: REMOVE_PROJECT_HIDDEN_LIST,
    projectID,
  }
}
