import { SET_NET_INFO } from './index'

export const setNetInfo = (isConnected) => {
  return {
    type: SET_NET_INFO,
    isConnected,
  }
}
