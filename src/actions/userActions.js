import { USER_LOGIN, USER_LOGOUT, USER_UPDATE_INFO } from './index'

export const userLogInAction = (payload) => {
  return {
    type: USER_LOGIN,
    payload,
  }
}

export const userLogOutAction = () => {
  return {
    type: USER_LOGOUT,
  }
}
