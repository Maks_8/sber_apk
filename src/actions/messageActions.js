import {SHOW_MESSAGE, HIDE_MESSAGE} from './index'

export const showMessage = (text, messageType) => {
  return {
    type: SHOW_MESSAGE,
    text,
    messageType,
  }
}

export const hideMessage = () => {
  return {
    type: HIDE_MESSAGE,
  }
}
