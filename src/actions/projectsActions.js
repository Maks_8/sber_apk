import { SET_PROJECTS_INFO, UPDATE_PROJECT } from './index'

export const setProjectsInfoAction = (projects) => {
  return {
    type: SET_PROJECTS_INFO,
    projects,
  }
}


export const updateProjectList = (projects) => {
  return {
    type: UPDATE_PROJECT,
    projects,
  }
}
