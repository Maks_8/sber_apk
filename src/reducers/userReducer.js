import { USER_LOGIN, USER_LOGOUT } from '../actions'

const initState = {
  login: false,
  user: {},
}

const userReducer = (state = initState, action) => {
  switch (action.type) {
    case USER_LOGIN:
      return {
        ...state,
        login: true,
        user: action.payload,
      }
    case USER_LOGOUT:
      return {
        ...state,
        login: false,
        user: {},
      }
    default:
      return state
  }
}

export default userReducer
