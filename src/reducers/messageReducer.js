import {SHOW_MESSAGE, HIDE_MESSAGE} from '../actions'

const initState = {
  show: false,
  text: '',
  messageType: 'danger',
}

const messageReducer = (state = initState, action) => {
  switch (action.type) {
    case SHOW_MESSAGE:
      return {
        ...state,
        show: true,
        text: action.text,
        messageType: action.messageType,
      }
    case HIDE_MESSAGE:
      return {
        ...state,
        show: false,
      }
    default:
      return state
  }
}

export default messageReducer
