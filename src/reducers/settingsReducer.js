import { ADD_PROJECT_HIDDEN_LIST, REMOVE_PROJECT_HIDDEN_LIST } from '../actions'

const initState = {
  hidden_projects: [],
}

const settingsReducer = (state = initState, action) => {
  const removeFromList = (id, list) => {
    return list.filter((itemId) => itemId !== id)
  }

  switch (action.type) {
    case ADD_PROJECT_HIDDEN_LIST:
      return {
        ...state,
        hidden_projects: [...state.hidden_projects, action.projectID],
      }
    case REMOVE_PROJECT_HIDDEN_LIST:
      return {
        ...state,
        hidden_projects: removeFromList(action.projectID, state.hidden_projects),
      }
    default:
      return state
  }
}

export default settingsReducer
