import { SET_NET_INFO } from '../actions'

const initState = {
  isConnected: undefined,
}

const appReducer = (state = initState, action) => {
  switch (action.type) {
    case SET_NET_INFO:
      return {
        ...state,
        isConnected: action.isConnected,
      }
    default:
      return state
  }
}

export default appReducer
