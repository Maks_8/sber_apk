import { combineReducers } from 'redux'
import userReducer from './userReducer'
import messageReducer from './messageReducer'
import appReducer from './appReducere'
import projectsReducer from './projectsReducer'
import settingsReducer from './settingsReducer'

const rootReducer = combineReducers({
  user: userReducer,
  message: messageReducer,
  app: appReducer,
  projects: projectsReducer,
  settings: settingsReducer,
})

export default rootReducer
