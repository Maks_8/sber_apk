import { SET_PROJECTS_INFO, UPDATE_PROJECT } from '../actions'

const initState = {
  projects: [
    {
      id: '1',
      title: 'Project1',
      servises: [
        {
          id: '1',
          title: 'service 1',
          type: 'VM',
          params: [
            {
              count: '5',
            },
          ],
        },
        {
          id: '2',
          title: 'service 2',
          type: 'empty',
          params: [
            {
              count: '5',
            },
          ],
        },
        {
          id: '3',
          title: 'service 3',
          type: 'HD',
          params: [
            {
              current: 8.89,
              max: 12.12,
            },
          ],
        },
      ],
    },
    {
      id: '2',
      title: 'Project2',
      servises: [
        {
          id: '4',
          title: 'service 1',
          type: 'HD',
          params: [
            {
              current: 20.89,
              max: 21.12,
              overload: true,
            },
          ],
        },
        {
          id: '5',
          title: 'service 2',
          type: 'empty',
          params: [
            {
              count: '5',
            },
          ],
        },
        {
          id: '6',
          title: 'service 3',
          type: 'CPU',
          params: [
            {
              count: '5',
            },
          ],
        },
        {
          id: '7',
          title: 'service 4',
          type: 'empty',
          params: [
            {
              count: '5',
            },
          ],
        },
        {
          id: '8',
          title: 'service 5',
          type: 'empty',
          params: [
            {
              count: '5',
            },
          ],
        },
      ],
    },
    {
      id: '3',
      title: 'Project3',
      servises: [
        {
          id: '4',
          title: 'service 1',
          type: 'HD',
          params: [
            {
              current: 8.89,
              max: 12.12,
            },
          ],
        },
        {
          id: '5',
          title: 'service 2',
          type: 'empty',
          params: [
            {
              count: '5',
            },
          ],
        },
        {
          id: '6',
          title: 'service 3',
          type: 'CPU',
          params: [
            {
              count: '5',
            },
          ],
        },
        {
          id: '7',
          title: 'service 4',
          type: 'empty',
          params: [
            {
              count: '5',
            },
          ],
        },
        {
          id: '8',
          title: 'service 5',
          type: 'empty',
          params: [
            {
              count: '5',
            },
          ],
        },
      ],
    },
  ],
}

const projectsReducer = (state = initState, action) => {
  switch (action.type) {
    case SET_PROJECTS_INFO:
      return {
        ...state,
        projects: action.projects,
      }
    case UPDATE_PROJECT:
      return {
        ...state,
        projects: action.projects,
      }
    default:
      return state
  }
}

export default projectsReducer
