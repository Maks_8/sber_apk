import Constants from 'expo-constants'
import * as SecureStore from 'expo-secure-store'

export const protocol = Constants.manifest.extra.PROTOCOL
export const host = Constants.manifest.extra.HOST

export const Authorization = async (login, password) => {
  //hackathon106 hef971215

  try {
    const response = await fetch(`${protocol}://${host}/api/login`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json;charset=utf-8',
      },
      body: JSON.stringify({ login: login, password: password }),
    })
    if (response.ok) {
      const result = await response.json()
      await SecureStore.setItemAsync('token', result.data.token)
      // console.log(await SecureStore.getItemAsync('token'))
      return 'success'
    } else if (response.status === 422) {
      console.log(response.json())
      return []
    } else {
      return []
    }
  } catch (e) {
    return []
  }
}

export const sendToken = async (data) => {
  const token = await SecureStore.getItemAsync('token')
  try {
    const response = await fetch(`${protocol}://${host}/api/subscribeDevice`, {
      method: 'POST',
      headers: {
        Authorization: `Bearer ${token}`,
        'Content-Type': 'application/json;charset=utf-8',
      },
      body: data,
    })
    console.log('response', response.status)
    if (response.ok) {
      return 'success'
    } else if (response.status === 422) {
      return []
    } else {
      return []
    }
  } catch (e) {
    return []
  }
}
